package com.mytest.test;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class CassetteCountersProccess {

	public static void main(String[] args) {
		String jsonArray = "{\"payments\":[{\"a\":\"11\",\"b\":\"21\",\"c\":\"34\",\"d\":\"0\"},{\"a\":\"54\",\"b\":\"66\",\"c\":\"21\",\"d\":\"76\"},{\"a\":\"34\",\"b\":\"23\",\"c\":\"43\",\"d\":\"88\"}]}";
		String counterCashOutValue = retriveCassetteCounters("CashOutCounters", "CASH_OUT", jsonArray);
		String counterAllInValue = retriveCassetteCounters("AllInCounters", "ALL_IN_1", jsonArray);
		String counterRejectValue = retriveCassetteCounters("RejectCounters", "REJECT", jsonArray);
		String counterRetractValue = retriveCassetteCounters("RetractCounters", "RETRACT", jsonArray);
		
		System.out.println("counterCashOutValue= " + counterCashOutValue);
		System.out.println("counterAllInValue= " + counterAllInValue);
		System.out.println("counterRejectValue= " + counterRejectValue);
		System.out.println("counterRetractValue= " + counterRetractValue);
	}

	private static String retriveCassetteCounters(String counterName, String subCounterName, String jsonArray) {
		JsonObject jsonObject2 = new Gson().fromJson(jsonArray, JsonObject.class);
		String key = "";
		int i = 0;
		JsonObject innerObj = new JsonObject();
		JsonObject innerObjCashOut = new JsonObject();
		StringBuilder joinBuilder = new StringBuilder();

		Map<String, String> testMap = new LinkedHashMap<String, String>();
		JsonArray paymentsArray = jsonObject2.getAsJsonArray("payments");
		for (JsonElement jsonElement : paymentsArray) {
			Set<Entry<String, JsonElement>> elemEntry = ((JsonObject) jsonElement).entrySet();
			Iterator<Entry<String, JsonElement>> itr = elemEntry.iterator();
			String finalVal = "";
			while (itr.hasNext()) {
				Entry<String, JsonElement> entry = itr.next();
				key = entry.getKey();
				JsonPrimitive valuePrim = entry.getValue().getAsJsonPrimitive();
				if (valuePrim.isString()) {
					finalVal = valuePrim.getAsString();
				}
				testMap.put(key, finalVal);
			}
			if (!(subCounterName.equals("CASH_OUT") || subCounterName.equals("REJECT"))) {
				if (testMap.get("d").equalsIgnoreCase("0")) {
					testMap.clear();
				} else {
					joinBuilder.append(StringUtils.join(testMap.values(), ';')).append(",");
				}
			} else {
				if (subCounterName.equals("CASH_OUT")) {
					innerObjCashOut.addProperty("CASH_OUT" + "_" + ++i, StringUtils.join(testMap.values(), ';'));
				}else {
					joinBuilder.append(StringUtils.join(testMap.values(), ';')).append(",");
				}
			}
		}
		if (subCounterName.equals("CASH_OUT")) {
			System.out.println("CASH_OUT = "+innerObjCashOut);
			//JsonElement jsonElement = innerObjCashOut.get("payments");
			innerObj.add(subCounterName, innerObjCashOut.getAsJsonObject());
		}else {
			joinBuilder.deleteCharAt(joinBuilder.length() - 1);
			innerObj.addProperty(subCounterName, joinBuilder.toString());
		}
		return innerObj.toString();
	}


}


