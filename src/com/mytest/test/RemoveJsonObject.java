package com.mytest.test;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class RemoveJsonObject {

	public static void main(String[] args) {

		String jsonArray = "{\"parameters\":{\"a\":\"11\",\"b\":\"21\",\"c\":\"34\",\"d\":\"0\"}}";
	    JsonObject jsonObject2 = new Gson().fromJson(jsonArray, JsonObject.class);		
	    String str = null;
	    str = jsonObject2.getAsJsonObject("parameters").remove("a").getAsString();
	    System.out.println(str);
	    System.out.println(jsonObject2);
	}
}
