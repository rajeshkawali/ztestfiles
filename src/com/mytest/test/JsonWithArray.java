package com.mytest.test;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class JsonWithArray {

	public static void main(String[] args) {
		String jsonArray = "{\"payments\":[{\"a\":\"11\",\"b\":\"21\",\"c\":\"34\",\"d\":\"0\"},{\"a\":\"54\",\"b\":\"66\",\"c\":\"21\",\"d\":\"76\"},{\"a\":\"34\",\"b\":\"23\",\"c\":\"43\",\"d\":\"88\"}]}";
		String counterCashOutValue = retriveCassetteCounters("CashOutCounters", "CASH_OUT", jsonArray);
		String counterAllInValue = retriveCassetteCounters("AllInCounters", "ALL_IN", jsonArray);
		System.out.println("counterCashOutValue= " + counterCashOutValue);
		System.out.println("counterAllInValue= " + counterAllInValue);
	}

	private static String retriveCassetteCounters(String counterName, String subCounterName, String jsonArray) {
		JsonObject jsonObject2 = new Gson().fromJson(jsonArray, JsonObject.class);
		String key = "";
		JsonObject innerObj = new JsonObject();
		StringBuilder joinBuilder = new StringBuilder();
		Map<String, String> testMap = new LinkedHashMap<String, String>();
		JsonArray paymentsArray = jsonObject2.getAsJsonArray("payments");
		for (JsonElement jsonElement : paymentsArray) {
			Set<Entry<String, JsonElement>> elemEntry = ((JsonObject) jsonElement).entrySet();
			Iterator<Entry<String, JsonElement>> itr = elemEntry.iterator();
			String finalVal = "";
			while (itr.hasNext()) {
				Entry<String, JsonElement> entry = itr.next();
				key = entry.getKey();
				JsonPrimitive valuePrim = entry.getValue().getAsJsonPrimitive();
				if (valuePrim.isString()) {
					finalVal = valuePrim.getAsString();
				}
				testMap.put(key, finalVal);
			}
			if (subCounterName.equals("ALL_IN")) {
				if (testMap.get("d").equalsIgnoreCase("0")) {
					System.out.println("Test=" + testMap.size() + "    d=" + testMap.get("d"));
					testMap.clear();
				} else {
					joinBuilder.append(StringUtils.join(testMap.values(), ';')).append(",");
				}
			} else {
				joinBuilder.append(StringUtils.join(testMap.values(), ';')).append(",");
			}
		}
		joinBuilder.deleteCharAt(joinBuilder.length() - 1);
		innerObj.addProperty("payments", joinBuilder.toString());
		return innerObj.toString();
	}
}
