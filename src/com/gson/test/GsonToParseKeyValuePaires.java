package com.gson.test;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class GsonToParseKeyValuePaires {

	public static void main(String[] args) {

		Gson gson = new Gson();
		String json = "{\"k1\":\"v1\",\"k2\":\"v2\"}";
		Map<String, Object> map = new HashMap<String, Object>();
		map = (Map<String, Object>) gson.fromJson(json, map.getClass());
		System.out.println(map);
		
		System.out.println("-----------------------------------------------------------------------");
		
		String json2 = "[{\"property\":\"surname\",\"direction\":\"ASC\"}]";
		Type listType = new TypeToken<ArrayList<HashMap<String, String>>>() {}.getType();
		Gson gson2 = new Gson();
		ArrayList<Map<String, String>> myList = gson2.fromJson(json2, listType);
		for (Map<String, String> m : myList) {
			System.out.println(m.get("property"));
			System.out.println(m.toString());
		}

	}

}
