package com.test.test;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Example to readJson using TreeModel
 */
public class ReadJson {

	public static void main(String[] args) {

		JsonParser parser = new JsonParser();
		JsonElement jsonElement = parser.parse("{\"message\":\"Hi\",\"place\":{\"name\":\"World!\"}}");
		JsonObject rootObject = jsonElement.getAsJsonObject();
		String message = rootObject.get("message").getAsString(); // get property "message"
		JsonObject childObject = rootObject.getAsJsonObject("place"); // get place object
		System.out.println(childObject);
		String place = childObject.get("name").getAsString(); // get property "name"
		System.out.println(message + " " + place); // print "Hi World!"*/

	}

}
