package com.test.test;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;



public class Snippet {

	public static void main(String[] args) {
		JsonParser parser = new JsonParser();
		String json = "{ \"f1\":\"Hello\",\"f2\":{\"f3:\":\"World\"}}";
		JsonElement jsonTree = parser.parse(json);
		if (jsonTree.isJsonObject()) {
			JsonObject jsonObject = jsonTree.getAsJsonObject();
			JsonElement f1 = jsonObject.get("f1");
			JsonElement f2 = jsonObject.get("f2");

			System.out.println("Outside if"+f1 +"-----"+f2);
			if (f2.isJsonObject()) {
				JsonObject f2Obj = f2.getAsJsonObject();
				JsonElement f3 = f2Obj.get("f3");
				System.out.println("Inside if"+f2Obj+"-----"+f3);
			}
		}
		//int i=1;
		//java.util.Map<String, Object> item = JavaConverters.mapAsJavaMapConverter(response).asJava();
		//JsonObject jsonObj = new JsonObject();
		//JsonObject innerObj = new JsonObject();
		//innerObj.addProperty("Reject_"+i, "");
		//jsonObj.add("SVFE", innerObj);
		
		
	}
}
