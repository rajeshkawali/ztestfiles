package com.test.test;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

public class Test {

	public static void main(String[] args) {

		String jsonArray = "{\"payments\":[{\"a\": \"0\", \"b\": \"1\", \"c\":\"0\"},{\"a\": \"1\", \"b\": \"1\",\"c\": \"3\"}]}";
		JsonObject jsonObject2 = new Gson().fromJson(jsonArray, JsonObject.class);
		
		
		String csv ="";
		int i =0;
		String key ="";
		JsonObject jsonObj = new JsonObject();
		JsonObject innerObj = new JsonObject();
		//innerObj.addProperty("Reject_"+i, "");
		//jsonObj.add("SVFE", innerObj);    || key.equalsIgnoreCase("count")
		JsonArray paymentsArray = jsonObject2.getAsJsonArray("payments");
		Map<String, String> testMap = new LinkedHashMap<String, String>();

		for (JsonElement jsonElement : paymentsArray) {
			++i;
			Set<Entry<String, JsonElement>> elemEntry = ((JsonObject) jsonElement).entrySet();
			Iterator<Entry<String, JsonElement>> itr = elemEntry.iterator();
			String finalVal ="";
			while (itr.hasNext()) {
				Entry<String, JsonElement> entry = itr.next();
				key = entry.getKey();
				if (!key.equalsIgnoreCase("2") ) {
					JsonPrimitive valuePrim = entry.getValue().getAsJsonPrimitive();
					if (valuePrim.isString()) {
						finalVal = valuePrim.getAsString();
						if (StringUtils.isEmpty(finalVal)) {
							continue;
						}
						if (key.equalsIgnoreCase("b")) {
							finalVal = valuePrim.getAsString();
						} else if(key.equalsIgnoreCase("c")){
							finalVal = valuePrim.getAsString();
						}else {
							finalVal = valuePrim.getAsString();
						}
					}
				}
				testMap.put(key, finalVal);
			}
			if(testMap.get("a").equalsIgnoreCase("0")) {
				testMap.clear();
				System.out.println("Test1="+testMap.size());
				//System.out.println("Test1="+testMap.get("a").toString());
			}
			System.out.println("Test2="+testMap.get("a"));
			innerObj.addProperty("Cash_Out"+"_"+i, StringUtils.join(testMap.values(), ';'));
		}
		
		System.out.println("finalVal="+innerObj.toString());
	}

}
