package com.test.test;

public class StringFormat {

	public static void main(String[] args) {
				
		String str = String.format("%03d", 1); // => "001"
//      │││   └── print the number one
//      ││└────── ... as a decimal integer
//      │└─────── ... minimum of 3 characters wide
//      └──────── ... pad with zeroes instead of spaces
		System.out.println(str);

	}

}
