package com.test.test;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class JsonArraytoJavaList {
       
        public static void main(String args[]){
                // Converting JSON String array to Java String array
                String jsonStringArray = "[\"JSON\",\"To\",\"Java\"]";
                //creating Gson instance to convert JSON array to Java array
                Gson converter = new Gson();
               
                Type type = new TypeToken<List<String>>(){}.getType();
                List<String> list =  converter.fromJson(jsonStringArray, type );
               
                //convert List to Array in Java
                String [] strArray = list.toArray(new String[0]);
               
                System.out.println("Java List created from JSON String Array - example");
                System.out.println("JSON String Array : " + jsonStringArray );
                System.out.println("Java List : " + list);
                System.out.println("String array : " + Arrays.toString(strArray));
               
                // let's now convert Java array to JSON array -        
                String toJson = converter.toJson(list);
                System.out.println("Json array created from Java List : " + toJson);
               
                // example to convert JSON int array into Java array and List of integer
                String json = "[101,201,301,401,501]";
               
                type = new TypeToken<List<Integer>>(){}.getType();
                List<Integer> iList = converter.fromJson(json, type);
                Integer[] iArray = iList.toArray(new Integer[0]);      
               
                System.out.println("Example to convert numeric JSON array to integer List and array in Java");
                System.out.println("Numeric JSON array : " + json);
                System.out.println("Java List of Integers : " + iList);
                System.out.println("Integer array in Java : " + Arrays.toString(iArray));
               
                // Again, let's convert this Java int array back to Json numeric array
                String toJsonInt = converter.toJson(iList);
                System.out.println("numeric JSON array create from Java collection : " + toJsonInt);
        }
}