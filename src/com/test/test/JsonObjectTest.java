package com.test.test;

import com.google.gson.Gson;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;

public class JsonObjectTest {

	public static void main(String[] args) {
		
		
		String jsonArray = "{\"payments\":[{\"a\": \"1\", \"b\": \"1\", \"c\": \"2\"},{\"a\": \"1\", \"b\": \"1\",\"b\": \"3\",\"c\":\"0\"}]}";
		
		
		String json = "{\"a\": 1, \"b\": 1}";
		JsonObject jsonObject = new Gson().fromJson(json, JsonObject.class);

		System.out.println(jsonObject); // {"a":1,"b":1}

		jsonObject.addProperty("a", 2);
		jsonObject.add("b", JsonNull.INSTANCE);
		System.out.println(jsonObject); // {"a":2,"b":null}

		jsonObject.remove("a");
		System.out.println(jsonObject); // {"b":null}
		

	}

}
