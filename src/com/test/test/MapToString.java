package com.test.test;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class MapToString {

	public static void main(String[] args) {
		
		
		final Map<String, String> testMap = new LinkedHashMap<String, String>();
		testMap.put("key1", "Denom");
		testMap.put("key2", "Curr");
		testMap.put("key3", "Count");
		
		String csv = StringUtils.join(testMap.values(), ';'); // Denom;Curr;Count
		System.out.println(csv);

	}

}
