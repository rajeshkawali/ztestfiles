package com.test.test;

public class IfElseTest {

	public static void main(String[] args) {
		String condition = "c";
		if(condition == "a") {
			System.out.println("a");
		} else if (condition == "b") {
			System.out.println("b");
		} else {
			System.out.println("c");
		}
		System.out.println("Out side the if else");
	}

}
