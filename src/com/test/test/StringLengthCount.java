package com.test.test;

public class StringLengthCount {

	public static void main(String[] args) {
		String rate = "      0.034  ";
		System.out.println("Using trim function==>"+rate.trim());
		
		String str = removeSpace(rate);
		System.out.println("Using removeSpace fun===>"+str);
	}

	public static String removeSpace(String s) {
	    String withoutspaces = "";
	    for (int i = 0; i < s.length(); i++) {
	        if (s.charAt(i) != ' ')
	            withoutspaces += s.charAt(i);

	    }
	    return withoutspaces;

	}
}
