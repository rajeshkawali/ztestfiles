package com.test.test;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class JsonAppender {

	public static void main(String[] args) {
		String jsonArray = "{\"payments\":[{\"a\": \"1\", \"b\": \"1\", \"c\":\"0\"},{\"a\": \"1\", \"b\": \"1\",\"c\": \"3\"}]}";
		JsonObject jsonObject2 = new Gson().fromJson(jsonArray, JsonObject.class);
		
		
		//String semiColon ="";
		int i =0;
		String key ="";
		JsonObject jsonObj = new JsonObject();
		JsonObject innerObj = new JsonObject();
		StringBuilder builder = new StringBuilder();
		StringBuilder joinBuilder = new StringBuilder();
		JsonArray paymentsArray = jsonObject2.getAsJsonArray("payments");
		Map<String, String> testMap = new LinkedHashMap<String, String>();

		for (JsonElement jsonElement : paymentsArray) {
			++i;
			Set<Entry<String, JsonElement>> elemEntry = ((JsonObject) jsonElement).entrySet();
			Iterator<Entry<String, JsonElement>> itr = elemEntry.iterator();
			String finalVal ="";
			String semiColon ="";
			while (itr.hasNext()) {
				Entry<String, JsonElement> entry = itr.next();
				key = entry.getKey();
				if (!key.equalsIgnoreCase("") ) {
					JsonPrimitive valuePrim = entry.getValue().getAsJsonPrimitive();
					if (valuePrim.isString()) {
						finalVal = valuePrim.getAsString();
						if (StringUtils.isEmpty(finalVal)) {
							continue;
						}
						if (key.equalsIgnoreCase("b")) {
							finalVal = "b";
						} else if(key.equalsIgnoreCase("c")){
							finalVal = "c";
						}else {
							finalVal = "a";
						}
					}
				}
				joinBuilder.append(semiColon).append(finalVal);
				semiColon=";";
			}
			joinBuilder.append(",");
			//innerObj.addProperty("Cash_Out"+"_"+i, joinBuilder.toString());
			//joinBuilder.delete(0,joinBuilder.length());
		}
		innerObj.addProperty("Cash_Out", joinBuilder.toString());
		System.out.println("finalVal="+innerObj.toString());
	}

}
