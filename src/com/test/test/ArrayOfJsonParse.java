package com.test.test;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

public class ArrayOfJsonParse {

	public static void main(String[] args) {

		String json = "{\"a\": 1, \"b\": 1}";
		JsonObject jsonObject = new Gson().fromJson(json, JsonObject.class);
		String jsonArray = "{\"payments\":[{\"a\": \"1\", \"b\": \"1\", \"c\":\"0\"},{\"a\": \"1\", \"b\": \"1\",\"b\": \"0\",\"c\": \"3\"}]}";
		JsonObject jsonObject2 = new Gson().fromJson(jsonArray, JsonObject.class);
		System.out.println(jsonObject2); // {"a":1,"b":1}
		// JsonArray jsonArray = "[{\"a\": 1, \"b\": 1, \"c\": 0},{\"a\": 1, \"b\":1,\"b\": 0,\"c\": 0}]";
		//System.out.println(jsonObject); // {"a":1,"b":1}
		//String json = "{\"result\":[{\"doc_no\":\"ES101\",\"itembarcode\":\"BRMS\",\"net_wt\":\"10\",\"gross_wt\":\"1\",\"stone_wt\":\"0\",\"stone_amt\":\"0\",\"rate\":\"32000\",\"making\":\"100\",\"qty\":\"1\",\"net_rate\":\"32100.0\",\"item_total\":\"32100.0\",\"sum_total\":\"64600.0\"},{\"doc_no\":\"ES101\",\"itembarcode\":\"MSAA0015\",\"net_wt\":\"10\",\"gross_wt\":\"11\",\"stone_wt\":\"100000\",\"stone_amt\":\"1\",\"rate\":\"32000\",\"making\":\"500\",\"qty\":\"1\",\"net_rate\":\"32500.0\",\"item_total\":\"32500.0\",\"sum_total\":\"64600.0\"}]}";
        
		JsonParser parser = new JsonParser();
		JsonObject rootObj = parser.parse(jsonArray).getAsJsonObject();
		JsonArray paymentsArray = rootObj.getAsJsonArray("payments");
		System.out.println(paymentsArray);
		for (JsonElement pa : paymentsArray) {
			System.out.println(pa);
			JsonObject paymentObj = pa.getAsJsonObject();
			String quoteid = paymentObj.get("a").getAsString();
			String dateEntered = paymentObj.get("b").getAsString();
			String amount = paymentObj.get("c").getAsString();
		}

		
		//JsonArray cassetteArray = jsonObject2.getAsJsonArray();
		Map<String, String> testMap = new LinkedHashMap<String, String>();
		StringBuilder builder = new StringBuilder();
		StringBuilder joinBuilder = new StringBuilder();
		//builder.append(";");
		JsonObject jsonObj = new JsonObject();
		JsonObject innerObj = new JsonObject();
		String semiColon ="";
		int i =0;
		for (JsonElement jsonElement : paymentsArray) {
			++i;
			Set<Entry<String, JsonElement>> elemEntry = ((JsonObject) jsonElement).entrySet();
			Iterator<Entry<String, JsonElement>> itr = elemEntry.iterator();
			String finalVal ="";
			while (itr.hasNext()) {
				Entry<String, JsonElement> entry = itr.next();
				String key = entry.getKey();
				if (!key.equalsIgnoreCase("") || key.equalsIgnoreCase("count")) {
					JsonPrimitive valuePrim = entry.getValue().getAsJsonPrimitive();
					if (valuePrim.isString()) {
						finalVal = valuePrim.getAsString();
						if (StringUtils.isEmpty(finalVal)) {
							continue;
						}
						if (key.equalsIgnoreCase("b")) {
							finalVal = "b";
						} else if(key.equalsIgnoreCase("c")){
							finalVal = "c";
						}else {
							finalVal = "a";
						}
					}
				}
				joinBuilder.append(semiColon).append(finalVal);
				semiColon=";";
				//testMap.put(finalVal, finalVal);
				//finalVal.join(finalVal+";");
				System.out.println("Hello while="+joinBuilder);
			}
			builder.append(joinBuilder+"_"+i);
			//csv = StringUtils.join(testMap.values(), ';');
			//System.out.println("Hello csv="+csv+"_"+i);
			System.out.println("Hello for joinBuilder="+joinBuilder);
			System.out.println("Hello for semiColon="+builder);
		}
		//System.out.println("Hello for end");
		
	}

}
